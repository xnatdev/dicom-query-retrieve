/*
 * dicom-query-retrieve: build.gradle
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

buildscript {
    ext {
        vXnat = "1.9.0"
    }
}

plugins {
    id "eclipse"
    id "idea"
    id "jacoco"
    id "java"
    id "maven-publish"
    id "com.dorongold.task-tree" version "1.5"
    id "com.palantir.git-version" version "0.12.1"
    id "io.franzbecker.gradle-lombok" version "4.0.0"
    id "io.spring.dependency-management" version "1.0.11.RELEASE"
    id "net.linguica.maven-settings" version "0.5"
    id "org.nrg.xnat.build.xnat-data-builder" version "${vXnat}"
}

group "org.nrg.xnatx.plugins"
version "2.1.0"
description "XNAT DICOM Query/Retrieve Plugin"

repositories {
    mavenLocal()
    maven { url "https://nrgxnat.jfrog.io/nrgxnat/libs-release" }
    maven { url "https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot" }
    mavenCentral()
    maven { url "https://jitpack.io" }
}

sourceCompatibility = 8
targetCompatibility = 8

compileJava {
    options.fork = false
}

sourceSets {
    main {
        java {
            srcDir "build/xnat-generated/src/main/java"
        }
        resources {
            srcDir "build/xnat-generated/src/main/resources"
        }
    }
}

configurations {
    all {
        exclude group: "junit"
        exclude group: "log4j"
        exclude group: "net.logstash.logback"
        exclude group: "org.nrg.xnat.pipeline"
        exclude group: "org.slf4j", module: "jcl-over-slf4j"
        exclude group: "org.slf4j", module: "log4j-over-slf4j"
        exclude group: "org.slf4j", module: "slf4j-log4j12"
        exclude group: "org.slf4j", module: "slf4j-simple"
    }
    implementation.extendsFrom(implementAndInclude)
}

jacocoTestReport {
    dependsOn test
    reports {
        xml.enabled = false
        csv.enabled = true
        html.enabled = true
    }
}

test {
    useJUnitPlatform {
        includeEngines "junit-jupiter"
        excludeEngines "junit-vintage"
    }

    minHeapSize = "512m"
    maxHeapSize = "4g"
}

dependencyManagement {
    imports {
        mavenBom "org.nrg:parent:${vXnat}"
    }
    generatedPomCustomization {
        enabled = false
    }
}

def vDcm4che = dependencyManagement.importedProperties["dcm4che.version"] as String
def vCommonsCli = dependencyManagement.importedProperties["commons-cli.version"] as String

// Use Mockito >= 3.x for mocking statics with JUnit 5 (where PowerMock is not available)
def vMockito = "4.+"

dependencies {
    implementation enforcedPlatform("org.nrg:parent:${vXnat}") {
        exclude group: "org.mockito"
    }

    implementation "org.nrg.xnat:web"
    implementation "org.nrg.dicom:dicom-edit4"
    implementation "org.nrg.dicom:dicom-edit6"
    implementation "org.nrg:dicom-xnat-util"
    implementation "org.nrg:dicom-xnat-mx"
    implementation "org.springframework:spring-web"
    implementation "org.springframework:spring-tx"
    implementation "org.springframework:spring-jms"
    implementation "org.springframework:spring-jdbc"
    implementation "org.springframework:spring-context"
    implementation "io.springfox:springfox-swagger2"
    implementation "dcm4che:dcm4che-core"
    implementation "dcm4che:dcm4che-net"
    implementation "org.hibernate:hibernate-core"
    implementation "org.hibernate:hibernate-envers"
    implementation "org.hibernate.javax.persistence:hibernate-jpa-2.1-api"
    implementation "com.fasterxml.jackson.core:jackson-core"
    implementation "com.fasterxml.jackson.core:jackson-annotations"
    implementation "com.fasterxml.jackson.core:jackson-databind"
    implementation "org.apache.commons:commons-lang3"
    implementation "org.aspectj:aspectjweaver"
    implementation "javax.validation:validation-api:1.1.0.Final"
    implementation "org.restlet:org.restlet"
    implementation "org.dcm4che:dcm4che-core"

    implementation("turbine:turbine") {
        transitive = false
    }

    implementAndInclude("dcm4che.tool:dcm4che-tool-dcmecho:${vDcm4che}") {
        transitive = false
    }
    implementAndInclude("dcm4che.tool:dcm4che-tool-dcmqr:${vDcm4che}") {
        transitive = false
    }
    implementAndInclude("commons-cli:commons-cli:${vCommonsCli}") {
        transitive = false
    }
    implementAndInclude("org.dcm4che:dcm4che-net") {
        transitive = false
    }
    implementAndInclude("org.dcm4che:dcm4che-json") {
        transitive = false
    }
    implementAndInclude("org.dcm4che:dcm4che-mime") {
        transitive = false
    }
    implementAndInclude("org.glassfish:jakarta.json:1.1.6") {
        transitive = false
    }

    compileOnly "javax.interceptor:javax.interceptor-api"

    testImplementation "org.junit.jupiter:junit-jupiter-api"
    testImplementation "org.springframework:spring-test"
    testImplementation "com.github.sbrannen:spring-test-junit5"
    testImplementation "org.mockito:mockito-core:${vMockito}"
    testImplementation "org.mockito:mockito-junit-jupiter:${vMockito}"
    testImplementation "org.assertj:assertj-core"
    testImplementation "com.h2database:h2"
    testImplementation "org.dcm4che:dcm4che-json"


    testRuntimeOnly "org.junit.jupiter:junit-jupiter-engine"
    testRuntimeOnly "ch.qos.logback:logback-classic"
}

// Pulls in the Jenkins BUILD_NUMBER environment variable if available.
def buildDate = new Date()
def buildNumber = System.getenv().BUILD_NUMBER?.toInteger() ?: "Manual"
def isDirty, branchName, gitHash, gitHashFull, commitDistance, lastTag, isCleanTag

try {
    def gitDetails = versionDetails()
    isDirty = gitVersion().endsWith ".dirty"
    branchName = gitDetails.branchName ?: "Unknown"
    gitHash = gitDetails.gitHash
    gitHashFull = gitDetails.gitHashFull
    commitDistance = gitDetails.commitDistance
    lastTag = gitDetails.lastTag
    isCleanTag = gitDetails.isCleanTag
} catch (IllegalArgumentException e) {
    logger.info "Got an error trying to read VCS metadata from git. It's possible this project is not under VCS control. Using placeholder values for manifest entries."
    isDirty = true
    branchName = "Unknown"
    gitHash = "None"
    gitHashFull = "None"
    commitDistance = 0
    lastTag = "None"
    isCleanTag = false
}

ext.gitManifest = manifest {
    attributes "Application-Name": project.description,
            "Build-Date": buildDate,
            "Build-Number": buildNumber,
            "Implementation-Version": project.version,
            "Implementation-Sha": gitHash,
            "Implementation-Sha-Full": gitHashFull,
            "Implementation-Commit": commitDistance,
            "Implementation-LastTag": lastTag,
            "Implementation-Branch": branchName,
            "Implementation-CleanTag": isCleanTag,
            "Implementation-Dirty": isDirty
}

logger.info """
Building artifacts with manifest attributes:

 * Build-Date:              ${buildDate}
 * Build-Number:            ${buildNumber}
 * Implementation-Version:  ${version}
 * Implementation-Sha-Full: ${gitHashFull}
 * Implementation-Sha:      ${gitHash}
 * Implementation-Commit:   ${commitDistance}
 * Implementation-LastTag:  ${lastTag}
 * Implementation-Branch:   ${branchName}
 * Implementation-CleanTag: ${isCleanTag}
 * Implementation-Dirty:    ${isDirty}
"""

lombok {
    version = dependencyManagement.importedProperties["lombok.version"] as String
    sha256 = dependencyManagement.importedProperties["lombok.checksum"] as String
}

task sourceJar(type: Jar, dependsOn: classes) {
    classifier "sources"
    manifest {
        from gitManifest
    }
    from sourceSets.main.allSource
}

task javadocJar(type: Jar) {
    classifier "javadoc"
    manifest {
        from gitManifest
    }
    from javadoc.destinationDir
}

jar {
    dependsOn test, sourceJar, javadocJar
    enabled = true
    manifest {
        from gitManifest
    }
}

task xnatPluginJar(type: Jar) {
    dependsOn test, sourceJar, javadocJar
    zip64 true
    archiveClassifier.set "xpl"
    manifest {
        from gitManifest
    }
    // files and folders with "-dev" or "--xx" in their name
    // will not be in the compiled jar
    exclude "**/resources/**/*-dev**"
    exclude "**/resources/**/*--xx**"
    from {
        configurations.implementAndInclude.collect { it.isDirectory() ? it : zipTree(it) }
    } {
        exclude "META-INF/*.SF"
        exclude "META-INF/*.DSA"
        exclude "META-INF/*.RSA"
    }
    with jar
}

publishing {
    publications {
        mavenJava(MavenPublication) {
            artifacts {
                artifact sourceJar
                artifact javadocJar
                artifact xnatPluginJar
            }

            pom.withXml {
                def root = asNode()
                root.appendNode("name", project.description)
                root.appendNode("url", "https://bitbucket.org/xnatdev/dicom-query-retrieve")
                root.appendNode("inceptionYear", "2019")

                root.appendNode("scm").with {
                    appendNode("url", "https://bitbucket.org/xnatdev/dicom-query-retrieve")
                    appendNode("connection", "scm:https://bitbucket.org/xnatdev/dicom-query-retrieve")
                    appendNode("developerConnection", "scm:git@bitbucket.org:xnatdev/dicom-query-retrieve.git")
                }

                root.appendNode("licenses").appendNode("license").with {
                    appendNode("name", "Simplified BSD 2-Clause License")
                    appendNode("url", "https://xnat.org/about/license.php")
                    appendNode("distribution", "repo")
                }

                root.appendNode("developers").with {
                    appendNode("developer").with {
                        appendNode("id", "mfmckay")
                        appendNode("name", "Mike McKay")
                        appendNode("email", "mfmckay@wustl.edu")
                    }
                    appendNode("developer").with {
                        appendNode("id", "rherrick")
                        appendNode("name", "Rick Herrick")
                        appendNode("email", "jrherrick@wustl.edu")
                    }
                    appendNode("developer").with {
                        appendNode("id", "hortonw")
                        appendNode("name", "Will Horton")
                        appendNode("email", "hortonw@wustl.edu")
                    }
                    appendNode("developer").with {
                        appendNode("id", "moore.c")
                        appendNode("name", "Charlie Moore")
                        appendNode("email", "moore.c@wustl.edu")
                    }
                }
            }
        }
    }
    repositories {
        maven {
            url "https://nrgxnat.jfrog.io/nrgxnat/libs-${project.version.endsWith("-SNAPSHOT") ? "snapshot" : "release"}"
            // The value for name must match <id> in ~/.m2/settings.xml
            name = "XNAT_Artifactory"
        }
    }
}

