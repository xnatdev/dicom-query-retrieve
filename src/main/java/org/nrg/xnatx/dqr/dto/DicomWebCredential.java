package org.nrg.xnatx.dqr.dto;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
public class DicomWebCredential {
    String aeTitle;
    String username;
    String password;
    boolean preemptiveAuth;
}
