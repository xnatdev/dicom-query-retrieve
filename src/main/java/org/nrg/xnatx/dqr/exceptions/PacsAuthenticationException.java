package org.nrg.xnatx.dqr.exceptions;

public class PacsAuthenticationException extends PacsException {
    public PacsAuthenticationException(final String message) {
        super(message);
    }
}
