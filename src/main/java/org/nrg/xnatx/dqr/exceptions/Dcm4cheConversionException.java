package org.nrg.xnatx.dqr.exceptions;

public class Dcm4cheConversionException extends Exception {
    public Dcm4cheConversionException(final String message, final Exception cause){
        super(message, cause);
    }
}
