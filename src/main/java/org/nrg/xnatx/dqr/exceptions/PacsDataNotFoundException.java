package org.nrg.xnatx.dqr.exceptions;

public class PacsDataNotFoundException extends PacsException {
    public PacsDataNotFoundException(final String message) {
        super(message);
    }
}
