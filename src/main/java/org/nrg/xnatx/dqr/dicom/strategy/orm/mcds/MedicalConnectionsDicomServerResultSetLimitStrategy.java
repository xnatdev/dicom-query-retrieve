/*
 * dicom-query-retrieve: org.nrg.xnatx.dqr.dicom.strategy.orm.mcds.MedicalConnectionsDicomServerResultSetLimitStrategy
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.dqr.dicom.strategy.orm.mcds;

import org.nrg.xnatx.dqr.dicom.strategy.orm.BasicResultSetLimitStrategy;

public class MedicalConnectionsDicomServerResultSetLimitStrategy extends BasicResultSetLimitStrategy {
}
