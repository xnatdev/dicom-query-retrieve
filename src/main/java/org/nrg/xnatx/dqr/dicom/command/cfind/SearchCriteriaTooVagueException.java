/*
 * dicom-query-retrieve: org.nrg.xnatx.dqr.dicom.command.cfind.SearchCriteriaTooVagueException
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.dqr.dicom.command.cfind;

import org.nrg.xnatx.dqr.exceptions.DqrRuntimeException;

public class SearchCriteriaTooVagueException extends DqrRuntimeException {

    private static final long serialVersionUID = 1L;
}
