/*
 * dicom-query-retrieve: PacsSessionFinder.js
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

/*jslint white: true, browser: true, vars: true */

console.log('PacsSessionFinder.js');

function PacsSessionFinder(sessionSearchFormId, sessionSelectionFormId, sessionSelectionStudyInstanceUidInputId, sessionSelectionPacsIdInputId, sessionSearchResultsDivId, terms) {
    "use strict";

    var that = this;

    this.constants = {
        "MODAL_WINDOW_NAME": "loadData",
        "CLIENT_ERROR_BAD_REQUEST": 400,
        "CLIENT_ERROR_NOT_FOUND": 404,
        "STUDY_DATE_COLUMN": 4,
        "OPEN_ROW_IMAGE": "row_details_open.png",
        "CLOSED_ROW_IMAGE": "row_details_close.png",
        "OPEN_ROW_ALT": "Show the series for this study",
        "CLOSED_ROW_ALT": "Hide the series for this study",
        "PROCESS_BUTTON_CLASS": "processButton",
        "PATIENT_TERM": terms.termForPatient,
        "STUDY_TERM": terms.termForStudy
    };

    this.sessionSearchFormId = sessionSearchFormId;

    this.sessionSelectionFormId = sessionSelectionFormId;

    this.sessionSelectionStudyInstanceUidInputId = sessionSelectionStudyInstanceUidInputId;

    this.sessionSelectionPacsIdInputId = sessionSelectionPacsIdInputId;

    this.sessionSearchResultsDivId = sessionSearchResultsDivId;

    this.activeElementBeforeSearch = null;

    this.currentPacsId = null;

    this.findSessions = function (pacsId) {
        this.currentPacsId = pacsId;


        XNAT.xhr.ajax({
            type: "POST",
            url: XNAT.url.csrfUrl("/xapi/pacs/" + pacsId + "/studies"),
            data: jq("#" + this.sessionSearchFormId).serialize(),
            dataType: "json",
            context: this,
            success: this.showSessionSearchResults,
            error: this.handleSessionSearchFailure
        });

        this.saveFocusedField();
        openModalPanel(this.constants.MODAL_WINDOW_NAME, "Loading data...");
    };

    this.showSessionSearchResults = function (data) {
        var sessionSearchResultsTableId = sessionSearchResultsDivId + "Table";

        jq("#" + sessionSearchResultsDivId).empty().html('<div class="friendlyForm"><h4>PACS Query Results</h4></div><table cellpadding="0" cellspacing="0" border="0" class="pacsSessionSearchResults xnat-table data-table compact" id="' + sessionSearchResultsTableId + '"/>');

        // var stringStartsWithFilter = new StringStartsWithFilter();

        var dataTableOptions = {
            "aaData": data,
            "aoColumns": [
                {
                    "bSearchable": false,
                    "bSortable": false,
                    "mData": null,
                    "sDefaultContent": '<i title="' + that.constants.OPEN_ROW_ALT + '" class="fa fa-plus-square rowDetailsExpander" style="color: green; font-size: 1.25em;" />'
                },
                {
                    "mData": "patient.id",
                    "sTitle": that.constants.PATIENT_TERM + " ID",
                    // "dqrCustomFilter": stringStartsWithFilter
                },
                {
                    "mData": function (source) {
                        return source.patient.name.lastNameCommaFirstName;
                    },
                    "sTitle": that.constants.PATIENT_TERM + " Name"
                },
                {
                    "mData": function (source) {
                        // return source.patient.birthDate;
                        return ageFormatter(source.patient.birthDate);
                    },
                    "sTitle": "Age",
                    // "dqrCustomFilter": stringStartsWithFilter
                },
                {
                    "mData": "patient.sex",
                    "sTitle": "Gender"
                },
                {
                    "mData": "studyId",
                    "sTitle": that.constants.STUDY_TERM + " ID",
                    "sClass": "max200"
                    // "dqrCustomFilter": stringStartsWithFilter
                },
                {
                    "mData": "accessionNumber",
                    "sTitle": "Accession Number",
                    "sClass": "max200"
                    // "dqrCustomFilter": stringStartsWithFilter
                },
                {
                    "mData": function (source) {
                        // return source.studyDate;
                        return dateFormatter(source.studyDate);
                    },
                    "sTitle": that.constants.STUDY_TERM + " Date",
                    // "dqrCustomFilter": stringStartsWithFilter
                },
                {
                    "mData": "studyDescription",
                    "sTitle": that.constants.STUDY_TERM + " Description",
                    "sClass": "max200"
                },
                {
                    "bSearchable": false,
                    "bSortable": false,
                    "mData": null,
                    "sDefaultContent": '<button class="' + that.constants.PROCESS_BUTTON_CLASS + '">Choose</button>'
                }
            ],
            "oLanguage": {
                "sInfoPostFix": ""
            },
            "iDisplayLength": 10,
            "aaSorting": [
                [that.constants.STUDY_DATE_COLUMN, "desc"]
            ]
        };

        if (data.limitedResultSetSize) {
            dataTableOptions.oLanguage.sInfoPostFix = "<br/><span style='color:red;'>These search results were limited to " + data.resultSetSize + " records to avoid overtaxing the PACS.  You may need to narrow your search to find what you're looking for.</span>";
        } else if (data.studyDateRangeLimitResults.limited) {
            dataTableOptions.oLanguage.sInfoPostFix = "<br/><span style='color:red;'>" + data.studyDateRangeLimitResults.limitExplanation + "</span>";
        }

        jq("#" + sessionSearchResultsTableId).dataTable(dataTableOptions);

        this.addColumnFilters(sessionSearchResultsTableId, dataTableOptions.aoColumns);

        this.bindRowExpansionHandler(sessionSearchResultsTableId);

        this.bindProcessButtonHandler(sessionSearchResultsTableId);

        $('table.dataTable').removeClass('dataTable');

        closeModalPanel(this.constants.MODAL_WINDOW_NAME);
        this.restoreFocusedField();
    };

    this.addColumnFilters = function (sessionSearchResultsTableId, dataTableColumns) {
        var filterHeaderRowId = "filterHeaderRow";
        jq("#" + sessionSearchResultsTableId).find('thead').append('<tr id="' + filterHeaderRowId + '" class="filter">');

        dataTableColumns.forEach(function(column){
            if (column.mData) {
                var inputId = filterHeaderRowId + "Input" + i;
                jq("#" + filterHeaderRowId).append('<th class="noPointer"><input type="text" id="' + inputId + '" name="' + inputId + '" placeholder="Filter..." class="filter_init" /></th>');
            } else {
                jq("#" + filterHeaderRowId).append('<th class="noPointer"/>');
            }
        });

        var asInitVals = [];

        jq("#" + sessionSearchResultsTableId + " thead input").each(function (i) {
            asInitVals[i] = this.value;
        });

        jq("#" + sessionSearchResultsTableId + " thead input").focus(function () {
            if (this.className === "filter_init") {
                this.className = "";
                this.value = "";
            }
        });

        jq("#" + sessionSearchResultsTableId + " thead input").blur(function () {
            if (this.value === "") {
                this.className = "filter_init";
                this.value = asInitVals[jq("#" + sessionSearchResultsTableId + " thead input").index(this)];
            }
        });

        jq("#" + sessionSearchResultsTableId + " thead input").keyup(function () {
            /* Filter on the column (the index) of this element, +1 to account for the row expander column */
            var columnIndexOfThisFilter = jq("#" + sessionSearchResultsTableId + " thead input").index(this) + 1;
            jq("#" + sessionSearchResultsTableId).dataTable().fnFilter(that.getFilterRegex(this.value, dataTableColumns[columnIndexOfThisFilter]), columnIndexOfThisFilter, true);
        });

        // we can't turn off filtering entirely on the table cause then our individual column filters won't work
        // so just hide the global (all-column) filter
        jq("#" + sessionSearchResultsTableId + "_filter").css("display", "none");
    };

    this.getFilterRegex = function (filterText, dataTableColumn) {
        if (dataTableColumn.dqrCustomFilter) {
            return dataTableColumn.dqrCustomFilter.getFilterRegex(filterText);
        }
        // no custom filter specified, use the default
        return new StringIndexOfFilter().getFilterRegex(filterText);
    };

    this.bindRowExpansionHandler = function (sessionSearchResultsTableId) {
        var rowExpansionHandler = function () {
            var nTr = jq(this).parents('tr')[0];
            var oTable = jq("#" + sessionSearchResultsTableId).dataTable();
            if (oTable.fnIsOpen(nTr)) {
                // This row is already open - close it

                $(this).removeClass('fa-minus-square').addClass('fa-plus-square').css('color','green');
                this.title = that.constants.OPEN_ROW_ALT;
                oTable.fnClose(nTr);
            } else {
                //Open this row

                // prevent overcaffeinated clicking of the image
                jq(this).removeClass("rowDetailsExpander");

                $(this).removeClass('fa-plus-square').addClass('fa-minus-square').css('color','#888');
                this.title = that.constants.CLOSED_ROW_ALT;
                var seriesRow = oTable.fnOpen(nTr, "Loading series...<img src=\"" + serverRoot + "/scripts/yui/build/assets/skins/images/wait.gif\"/>", 'rowDetailsExpanding');
                var pacsSeriesFinder = new PacsSeriesFinder(oTable.fnGetData(nTr), jq(seriesRow).children().first(), this, rowExpansionHandler, that.currentPacsId);
                pacsSeriesFinder.findSeries();
            }
        };
        jq("#" + sessionSearchResultsTableId).on("click", ".rowDetailsExpander", rowExpansionHandler);
    };

    this.bindProcessButtonHandler = function (sessionSearchResultsTableId) {
        var processButtonHandler = function () {
            var nTr = jq(this).parents('tr')[0];
            var oTable = jq("#" + sessionSearchResultsTableId).dataTable();
            var study = oTable.fnGetData(nTr);
            jq("#" + that.sessionSelectionStudyInstanceUidInputId).val(study.studyInstanceUid);
            jq("#" + that.sessionSelectionPacsIdInputId).val(that.currentPacsId);
            concealContent();
            jq("#" + that.sessionSelectionFormId).submit();
        };
        jq("#" + sessionSearchResultsTableId).on("click", "button.processButton", processButtonHandler);
    };

    this.handleSessionSearchFailure = function (jqXHR) {
        closeModalPanel(this.constants.MODAL_WINDOW_NAME);
        this.restoreFocusedField();
        var errorMsg, errorTitle = 'Could not complete search';

        if (this.constants.CLIENT_ERROR_BAD_REQUEST === jqXHR.status) {
            errorMsg = "Please specify at least one of the search criteria.";
        } else if (this.constants.CLIENT_ERROR_NOT_FOUND === jqXHR.status) {
            errorMsg = "There were no results found that match this search criteria.";
            errorTitle = "Nothing to display";
        } else {
            errorMsg = "Error " + jqXHR.status + ": " + jqXHR.statusText;
        }
        XNAT.dialog.message({ title: errorTitle, content: spawn('p',errorMsg) });
    };

    this.saveFocusedField = function () {
        this.activeElementBeforeSearch = document.activeElement;
    };

    this.restoreFocusedField = function () {
        if (this.activeElementBeforeSearch) {
            this.activeElementBeforeSearch.focus();
        }
    };

    var dateFormatter = function (oData) {
        if (!oData) {
            return "";
        }
        // all dates from PACS are returned as YYYYMMDD and must be parsed before they can be processed as date objects.
        var ydate;
        if (oData.length === 8) {
            ydate = oData.toString();
            ydate = oData.slice(0,4)+'-'+oData.slice(4,6)+'-'+oData.slice(6,8);
        }

        var oDate = new Date(ydate);

        return oDate.toISOString().substring(0,10);
    };

    var ageFormatter = function (dob) {
        if (!dob) {
            return "";
        }
        // all dates from PACS are returned as YYYYMMDD and must be parsed before they can be processed as date objects.
        var ydate;
        if (dob.length === 8) {
            ydate = dob.toString();
            ydate = dob.slice(0,4)+'-'+dob.slice(4,6)+'-'+dob.slice(6,8);
        }

        var dCurrent = new Date();
        var dDob = new Date(ydate);
        var ageInMilliSeconds = Math.abs(dCurrent - dDob);
        var milliSecondsInAYear = 31556900000;
        return Math.floor(ageInMilliSeconds / milliSecondsInAYear);
    };
}
