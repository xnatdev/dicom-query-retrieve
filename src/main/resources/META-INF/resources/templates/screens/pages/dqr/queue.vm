#* @vtlvariable name="user" type="org.nrg.xdat.security.XDATUser" *#
#* @vtlvariable name="displayManager" type="org.nrg.xdat.display.DisplayManager" *#
#* @vtlvariable name="data" type="org.apache.turbine.util.RunData" *#
#* @vtlvariable name="turbineUtils" type="org.nrg.xdat.turbine.utils.TurbineUtils" *#
#* @vtlvariable name="siteConfig" type="org.nrg.xdat.preferences.SiteConfigPreferences" *#
#* @vtlvariable name="contextService" type="org.nrg.framework.services.ContextService" *#
#* @vtlvariable name="content" type="org.apache.turbine.services.pull.tools.ContentTool" *#
#* @vtlvariable name="project" type="org.nrg.xdat.om.XnatProjectdata" *#

#set ($SITE_ROOT = $content.getURI(""))
#set ($projectId = $project.id)
#set ($userHelper = $data.getSession().getAttribute("userHelper"))

#set ($subjectDisplay = "$!displayManager.getSingularDisplayNameForSubject()")
#set ($sessionDisplay = "$!displayManager.getSingularDisplayNameForImageSession()")
<script>
    window.subjectDisplay      = '$!subjectDisplay' || 'Subject';
    window.sessionDisplay      = '$!sessionDisplay' || 'Session';
    window.subjectDisplayLower = window.subjectDisplay.toLowerCase();
    window.sessionDisplayLower = window.sessionDisplay.toLowerCase();
</script>

#if ($turbineUtils.isBlankString($projectId))
    #set ($projectId = $turbineUtils.escapeHTML($!data.getParameters().getString('project')))
#end

#set ($pacsId = $!turbineUtils.escapeHTML($!data.getParameters().getString('pacs')))
#set ($pacsLabel = $!turbineUtils.escapeHTML($!data.getParameters().getString('label')))
<script>
    window.projectId    = '$!{projectId}';
    window.selectedPacs = '$!{pacsId}';
    window.pacsId       = window.selectedPacs;
    window.pacsLabel    = '$!{pacsLabel}';
</script>

#set ($role = $!turbineUtils.escapeHTML($!data.getParameters().getString('role')))

<!-- role: $!role -->

#set ($isAllowed = false)

#if ($user.checkRole("Administrator") || $user.checkRole("Dqr") || ($contextService && $contextService.getBean("dqrPreferences").getAllowAllUsersToUseDqr()))
    #set ($isAllowed = true)
#end

## block a non-admin from viewing the admin view
#if (!$user.checkRole("Administrator") && $role == 'admin')
    #set ($isAllowed = false)
#end

#if ($isAllowed)

<script>
    window.isAllowed = true;
    window.adminView = window.isAdmin && '$!role' === 'admin';
</script>

<style type="text/css">
    table.click-rows tr:hover { cursor: pointer }
    table.click-rows tr:active {
        cursor: pointer;
        background-color: #c2e7ff !important;
    }
</style>

<div id="page-body">
    <div class="pad">

        <header id="content-header">
            <div class="pad" style="padding:20px 0;">
                <h2 class="pull-left" style="margin:0;">
                    PACS Import Queue and History
                </h2>
                #if ($user.checkRole("Administrator"))

                    ## toggle the 'role' param to switch between views
                    <div class="pull-right align-right" id="switch-queue-history-view">
                        <p>
                            <label>View import queue and history for:
                                <select id="switch-view-menu" class="hidden" style="width:150px;">
                                    <option value="user" #if($role != 'admin')selected#end>current user</option>
                                    <option value="admin" #if($role == 'admin')selected#end>all users</option>
                                </select>
                            </label>
                        </p>
                    </div>

                    #[[
                    <script>
                        (function(XNAT){

                            var switchViewMenu = $('#switch-view-menu');

                            switchViewMenu.on('change', function(e){
                                e.preventDefault();
                                console.log(this.value);
                                var tabValue = getUrlHashValue('tab=');
                                var urlHash = tabValue ? XNAT.url.updateHashQuery(window.location.hash, 'tab', tabValue, /\/*#\/*|#+/) : '';
                                window.location.href = XNAT.url.rootUrl('/app/template/Page.vm?view=dqr/queue&role=' + this.value + urlHash);
                            });

                            menuInit(switchViewMenu, {}, 150);

                            // var switchViewContainer = document.getElementById('switch-queue-history-view');

                            // var USER = window.username;
                            // var ALL = 'all users';
                            // var roleParam = window.adminView ? 'dqr' : 'admin';

                            // switchViewContainer.querySelector('.username').textContent = window.adminView ? ALL : USER;
                            // switchViewContainer.querySelector('.which-view').textContent = window.adminView ? USER : ALL;
                            // ## switchViewContainer.querySelector('#switch-view-link').setAttribute('href', '${SITE_ROOT}/')

                        })(getObject(window.XNAT))
                    </script>
                    ]]#

                #end
                <div class="clearfix clear"></div>
            </div>
        </header>

        <div id="pacs-queue-history-tabs">
            <div class="content-tabs xnat-tab-container" style="display: block;">

                <div class="xnat-nav-tabs top xnat-tab-nav">
                    <ul class="tab-group">
                        <li class="tab active" data-tab="queue"><a title="Queue" href="#!">Queue</a></li>
                        <li class="tab" data-tab="history"><a title="History" href="#!">History</a></li>
                    </ul>
                </div>

                <div class="xnat-tab-content">
                    <div class="tab-pane active" data-name="queue" data-tab="queue">
                        <div id="pacs-import-queue-display">
                            <p class="loading"></p>
                            <script>
                                // if it takes more than two seconds to get the data,
                                // the 'loading' text should show
                                window.setTimeout(function(){
                                    $('#pacs-import-queue-display').find('p.loading').text('Loading data...');
                                }, 2000)
                            </script>
                            <!-- PACS import queue table will render here -->
                        </div>
                    </div>
                    <div class="tab-pane" data-name="history" data-tab="history">
                        <div id="pacs-import-history-display">
                            <p class="loading"></p>
                            <script>
                                // if it takes more than two seconds to get the data,
                                // the 'loading' text should show
                                window.setTimeout(function(){
                                    $('#pacs-import-history-display').find('p.loading').text('Loading data...');
                                }, 2000)
                            </script>
                            <!-- PACS import history table will render here -->
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<link rel="stylesheet" type="text/css" src="${SITE_ROOT}/style/dqr/dqr.css" />
<script src="${SITE_ROOT}/scripts/xnat/plugin/dqr/queue.js"></script>

#else

<div class="error">Not authorized. Redirecting...</div>
<script>
    window.setTimeout(function(){
        window.location.href = '${SITE_ROOT}/'
    }, 2000)
</script>

#end
