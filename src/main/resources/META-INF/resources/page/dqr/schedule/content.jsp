<%--
  ~ dicom-query-retrieve: content.jsp
  ~ XNAT http://www.xnat.org
  ~ Copyright (c) 2005-2020, Washington University School of Medicine
  ~ All Rights Reserved
  ~
  ~ Released under the Simplified BSD.
  --%>

<%@ page contentType="text/html" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%-- this file is a placeholder for loading from '/page/#/dqr/schedule/#pacs=1/#label=PACS1' --%>
<%@ include file="view.html.jsp" %>